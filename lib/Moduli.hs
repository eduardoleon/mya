module Moduli where

expMod :: (Integral a) => a -> a -> a -> a
expMod m n x = go n
    where
        go 0 = 1
        go 1 = x `mod` m
        go n
          | even n = squareMod $ go (n `div` 2)
          | otherwise = x *% (squareMod $ go (n `div` 2))
        x *% x' = (x * x') `mod` m
        squareMod x = x *% x
