{-# LANGUAGE FlexibleContexts #-}

module Powers.Squares where 

import Control.Monad.Loops
import Control.Monad
import Data.Bits
import Control.Monad.State.Strict

converge :: (Eq a) => a -> (a -> a) -> a
converge = flip $ until =<< ((==) =<<)

convergeM :: (Monad m, Eq a) => a -> (a -> m a) -> m a
convergeM x f = do
    x' <- f x
    if x /= x'
       then convergeM x' f
       else return x'

integerSqrt :: (Integral a, Bits a) => a -> Maybe a
integerSqrt n
    | isPossibleSquare n = if n'^2 == n
                              then Just $ n'
                              else Nothing
    | otherwise = Nothing
    where
        n' = unsafeIntegerSqrt n

unsafeIntegerSqrt :: (Integral a, Bits a) => a -> a
unsafeIntegerSqrt n = converge (1 `shiftL` (floorLog2 n `div` 2)) newton
    where
        newton x = (x + (n `div` x)) `div` 2

isPerfectSquare n = isPossibleSquare n && ((unsafeIntegerSqrt n)^2 == n)

floorLog2 :: (Integral a, Bits a) => a -> Int
floorLog2 = popCount . shiftFill
    where
        shiftFill n = (`evalState` 1) . convergeM n $ \n -> do
            b <- get
            modify' (*2)
            return $ n .|. n `shiftR` b

isPossibleSquare n
    | even n =  isPossibleEvenSquare n
    | otherwise = isPossibleOddSquare n
-- all odd squares are of the form 8n + 1.
isPossibleOddSquare n = (n `mod` 8) == 1
isPossibleEvenSquare n = (n `mod` 4) == 0
