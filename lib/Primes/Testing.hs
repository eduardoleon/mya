{-# LANGUAGE BangPatterns #-}
module Primes.Testing (isPrime) where

import Data.List
import Control.Monad.State.Strict
import Control.Arrow
import Data.Maybe
import Data.Bits

import Moduli

isPrime :: (Bits a, Integral a) => a -> Bool
isPrime = mrpt 2

mrpt :: (Bits a, Integral a) => a -> a -> Bool
mrpt b p = maybe False (== 1) (listToMaybe list) || elem p' list
    where 
        p' = p - 1
        s = trailingZeroes p'
        d = p' `unsafeShiftR` s
        list = genericTake s . iterate (expMod p 2) $ expMod p d b

trailingZeroes :: (Integral a, Bits a, Integral b) => a -> b
trailingZeroes x = fromIntegral . popCount $ (x .&. (-x)) - 1
